package ru.app_creator.demo;

import android.app.Application;
import android.content.Context;

import com.orhanobut.logger.Logger;

import io.realm.Realm;
import ru.app_creator.demo.injection.component.ApplicationComponent;
import ru.app_creator.demo.injection.component.DaggerApplicationComponent;
import ru.app_creator.demo.injection.module.ApplicationModule;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class DeliveryApplication extends Application {
    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.init();
        Realm.init(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/HeliosCondRegular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public static DeliveryApplication get(Context context) {
        return (DeliveryApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        if (mApplicationComponent == null) {
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }
        return mApplicationComponent;
    }
}

package ru.app_creator.demo.util;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import ru.app_creator.demo.R;
import ru.app_creator.demo.data.model.ItemModel;

public class DialogFactory {

    /**
     * Интерфейс для событий
     */
    public interface OnDialogEventListener {
        void onRetry();
        void onFinish();
    }

    /**
     * Простое сообщение об ошибке
     *
     * @param context
     * @param throwable
     * @param onDialogEventListener
     */
    public static void error(Context context, Throwable throwable, final OnDialogEventListener onDialogEventListener) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.AlertDialogStyle);
        dialog.setTitle("Внимание");
        dialog.setCancelable(false);
        if (isServerException(throwable)) {
            dialog.setMessage("В настоящее время возникла техническая проблема. Мы работаем над ее решением. Повторите попытку позже. Спасибо за понимание.");
        } else {
            dialog.setMessage("Для работы приложения необходимо подключение к сети интернет");
            dialog.setPositiveButton("Повторить", (dialog1, which) -> {
                dialog1.dismiss();
                if (onDialogEventListener != null) {
                    onDialogEventListener.onRetry();
                }
            });
        }
        dialog.setNegativeButton("Закрыть", (dialog1, which) -> {
            dialog1.dismiss();
            if (onDialogEventListener != null) {
                onDialogEventListener.onFinish();
            }
        });
        dialog.show();
    }

    /**
     * Проверка на тип ошибки
     *
     * @param throwable
     * @return
     */
    public static boolean isServerException(Throwable throwable) {
        return  (throwable.getClass() == retrofit2.adapter.rxjava.HttpException.class
                || throwable.getClass() == com.google.gson.stream.MalformedJsonException.class
                || throwable.getClass() == io.realm.exceptions.RealmPrimaryKeyConstraintException.class);

    }

    /**
     * Простоте сообщение
     *
     * @param context
     * @param message
     */
    public static void showMessage(Context context, String message) {
        Snackbar snack = Snackbar.make(((Activity) context).findViewById(R.id.content), message, Snackbar.LENGTH_SHORT);
        ViewGroup group = (ViewGroup) snack.getView();
        group.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        snack.show();
    }
}

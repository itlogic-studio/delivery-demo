package ru.app_creator.demo.util;

public class Helper {

    /**
     * Форматирование цены
     *
     * @param currency
     * @param value
     * @return
     */
    public static String formatCurrency(String currency, Double value) {
        if ((value == Math.floor(value)) && !Double.isInfinite(value))
            return String.format("%.0f " + currency, value);
        else
            return String.format("%.2f " + currency, value);
    }
}

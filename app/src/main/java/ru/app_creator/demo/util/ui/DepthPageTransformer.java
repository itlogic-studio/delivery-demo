package ru.app_creator.demo.util.ui;

import android.support.v4.view.ViewPager;
import android.view.View;

public class DepthPageTransformer implements ViewPager.PageTransformer {

    private static float MIN_SCALE = 0.75f;

    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();

        if (position <= -1) {
            view.setAlpha(0);
            view.setVisibility(View.GONE);
        } else if (position <= 0) {
            view.setAlpha(1);
            view.setTranslationX(0);
            view.setScaleX(1);
            view.setScaleY(1);
            if (view.getVisibility() != View.VISIBLE)
                view.setVisibility(View.VISIBLE);
        } else if (position <= 1) {
            view.setAlpha(1 - position);
            view.setTranslationX(pageWidth * -position);

            float scaleFactor = MIN_SCALE + (1 - MIN_SCALE) * (1 - Math.abs(position));
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);
            if (position == 1) {
                view.setVisibility(View.GONE);
            } else {
                if (view.getVisibility() != View.VISIBLE)
                    view.setVisibility(View.VISIBLE);
            }
        } else {
            view.setAlpha(0);
            view.setVisibility(View.GONE);
        }
    }
}

package ru.app_creator.demo.ui.view;

import java.util.List;

import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.ui.base.MvpView;

public interface ViewMvpView extends MvpView {

    void showItem(ItemModel item, List<ItemModel> recommends);
    void addBasketEvent(int count);
    void updateBasketItemCount(int count);
}

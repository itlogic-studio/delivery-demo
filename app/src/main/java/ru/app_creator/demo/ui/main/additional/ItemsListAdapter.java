package ru.app_creator.demo.ui.main.additional;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.app_creator.demo.R;
import ru.app_creator.demo.data.event.ItemClickEvent;
import ru.app_creator.demo.data.model.ImageModel;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.util.Helper;

public class ItemsListAdapter extends RecyclerView.Adapter<ItemsListAdapter.ViewHolder> {

    private List<ItemModel> dataSet;
    private int resource_id;
    private Context context;

    @Inject
    public ItemsListAdapter() {
        dataSet = new ArrayList<>();
    }

    public void init(Context context, int resource_id) {
        this.context = context;
        this.resource_id = resource_id;
    }

    public void setDataSet(List<ItemModel> data) {
        dataSet = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(resource_id, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemModel item = dataSet.get(position);
        holder.textName.setText(item.getName());
        holder.textDesc.setText(item.getDesc());
        holder.textPrice.setText((item.getPrice() == 0) ? "Бесплатно" : Helper.formatCurrency("руб.", item.getPrice()));
        holder.btnBasket.setText((item.getShow_detail() == 1) ? "Открыть" : "Купить");
        List<ImageModel> images = item.getImages();
        if (images.size() > 0) {
            Glide.with(context)
                    .load(images.get(0).getUrl_medium())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(holder.image);
        }
        holder.btnBasket.setOnClickListener(view -> EventBus.getDefault().post(new ItemClickEvent(item)));
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    /**
     * ViewHolder
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textName) TextView textName;
        @BindView(R.id.textDesc) TextView textDesc;
        @BindView(R.id.textPrice) TextView textPrice;
        @BindView(R.id.textCount) TextView textCount;
        @BindView(R.id.image) ImageView image;
        @BindView(R.id.btnBasket) Button btnBasket;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

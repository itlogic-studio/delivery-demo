package ru.app_creator.demo.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import ru.app_creator.demo.data.SyncService;
import ru.app_creator.demo.ui.base.BaseActivity;
import ru.app_creator.demo.ui.main.MainActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!SyncService.isRunning(this)) {
            startService(SyncService.getStartIntent(this));
        }
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}

package ru.app_creator.demo.ui.view.additional;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import ru.app_creator.demo.R;
import ru.app_creator.demo.data.model.ImageModel;
import ru.app_creator.demo.injection.ActivityContext;

public class ImageSliderAdapter extends PagerAdapter {

    private List<ImageModel> dataSet;
    private Context context;

    @Inject
    public ImageSliderAdapter(@ActivityContext Context context) {
        this.context = context;
        dataSet = new ArrayList<>();
    }

    public void setDataSet(List<ImageModel> data) {
        dataSet = data;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final View view = LayoutInflater.from(context).inflate(R.layout.item_image_slider, container, false);
        Glide.with(context)
                .load(dataSet.get(position).getUrl_large())
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        return false;
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into((ImageView) view.findViewById(R.id.image));
        container.addView(view, ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
        return view;
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}

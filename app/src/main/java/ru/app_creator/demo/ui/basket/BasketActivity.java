package ru.app_creator.demo.ui.basket;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.app_creator.demo.R;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.ui.base.BaseActivity;
import ru.app_creator.demo.ui.basket.additional.BasketAdapter;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BasketActivity extends BaseActivity implements BasketMvpView {

    @Inject BasketPresenter mBasketPresenter;
    @Inject BasketAdapter adapter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.emptyList) View emptyList;
    @BindView(R.id.wrapOrderResume) View wrapOrderResume;
    @BindView(R.id.items_list) RecyclerView itemsList;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, BasketActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_basket);
        ButterKnife.bind(this);
        mBasketPresenter.attachView(this);
        setupUI();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    private void setupUI() {
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = ContextCompat.getDrawable(this, R.mipmap.ic_back_arrow_white);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        toolbar.setNavigationOnClickListener(v -> finish());
        showView(emptyList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        itemsList.setHasFixedSize(true);
        itemsList.setLayoutManager(layoutManager);
        itemsList.setAdapter(adapter);
    }

    @Override
    public void showData(List<ItemModel> items) {
        hideView(emptyList);
        adapter.setDataSet(items);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showDataEmpty() {
        showView(emptyList);
    }
}

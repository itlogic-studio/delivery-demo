package ru.app_creator.demo.ui.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.app_creator.demo.R;
import ru.app_creator.demo.data.event.ItemClickEvent;
import ru.app_creator.demo.data.event.ServiceErrorEvent;
import ru.app_creator.demo.data.model.GroupModel;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.ui.base.BaseActivity;
import ru.app_creator.demo.ui.basket.BasketActivity;
import ru.app_creator.demo.ui.main.additional.PageAdapter;
import ru.app_creator.demo.ui.view.ViewActivity;
import ru.app_creator.demo.util.DialogFactory;
import ru.app_creator.demo.ui.main.additional.RecommendDialogFactory;

public class MainActivity extends BaseActivity implements MainMvpView {

    @Inject MainPresenter mMainPresenter;
    @Inject RecommendDialogFactory recommendDialog;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.viewPager) ViewPager pages;
    @BindView(R.id.tabs) TabLayout tabLayout;
    @BindView(R.id.basket) View basketBtn;
    @BindView(R.id.textCount) TextView itemCountText;

    PageAdapter mPageAdapter;
    private int currentPage = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mPageAdapter = new PageAdapter(getSupportFragmentManager());
        mMainPresenter.attachView(this);
        mMainPresenter.loadData();
        setUpUI();
        if (savedInstanceState != null) {
            currentPage = savedInstanceState.getInt("currentPage", 0);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentPage", pages.getCurrentItem());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    /**
     * Событие
     * Ошибка с сервиса синхронизации
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ServiceErrorEvent event) {
        DialogFactory.error(MainActivity.this, event.getError(), new DialogFactory.OnDialogEventListener() {
            @Override
            public void onRetry() {}

            @Override
            public void onFinish() {}
        });
    }

    /**
     * Событие
     * Клик по айтему
     *
     * @param event
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ItemClickEvent event) {
        if (event.getItem().getShow_detail() == 1) {
            startActivityForResult(ViewActivity.getStartIntent(this, event.getItem().getId()), 100);
        } else {
            mMainPresenter.getAdditional(event.getItem());
        }
    }

    /**
     * Настройка UI
     */
    private void setUpUI() {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        pages.setAdapter(mPageAdapter);
        tabLayout.setupWithViewPager(pages);
    }

    @OnClick(R.id.basket)
    public void submit() {
        startActivityForResult(BasketActivity.getStartIntent(this), 100);
    }

    /**
     * Покажем данные
     *
     * @param data
     */
    @Override
    public void showData(List<GroupModel> data) {
        tabLayout.setVisibility(View.VISIBLE);
        mPageAdapter.setDataSet(data);
        mPageAdapter.notifyDataSetChanged();
        pages.setCurrentItem(currentPage);
    }

    /**
     * Данных нет, покажем заглушку
     */
    @Override
    public void showDataEmpty() {
        tabLayout.setVisibility(View.GONE);
    }

    /**
     * Диалог выбора рекомендованых
     * товаров и кол-ва
     *
     * @param item
     * @param recommendItems
     */
    @Override
    public void showAdditional(ItemModel item, List<ItemModel> recommendItems) {
        recommendDialog.show(item, recommendItems, (count, recommend) -> {
            mMainPresenter.addToBasket(item, count, recommend);
        });
    }

    /**
     * Добавили в корзину, обновим индикатор
     *
     * @param count
     */
    @Override
    public void addBasketEvent(int count) {
        DialogFactory.showMessage(this,"Товары добавлены в корзину - " + String.valueOf(count) + " шт.");
    }

    @Override
    public void updateBasketItemCount(int count) {
        itemCountText.setText(String.valueOf(count));
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        recommendDialog.dismissDialog();
        super.onDestroy();
    }
}

package ru.app_creator.demo.ui.main;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.RealmResults;
import ru.app_creator.demo.data.DataManager;
import ru.app_creator.demo.data.model.GroupModel;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.injection.ConfigPersistent;
import ru.app_creator.demo.ui.base.BasePresenter;
import ru.app_creator.demo.util.RxUtil;
import rx.Subscription;
import rx.functions.Func1;

@ConfigPersistent
public class MainPresenter extends BasePresenter<MainMvpView> {

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject public MainPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(MainMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }

    /**
     * Получим список групп
     */
    public void loadData() {
        checkViewAttached();
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.getRealmHelper().getAllGroup()
                .filter(groupModels -> groupModels.isLoaded())
                .subscribe(groupModels -> {
                    if (groupModels.isEmpty()) {
                        getMvpView().showDataEmpty();
                    } else {
                        getMvpView().showData(groupModels);
                        getMvpView().updateBasketItemCount(mDataManager.getRealmHelper().getBasketCount());
                    }
                });
    }

    /**
     * Проверка на наличие рекомендуемых товаров
     *
     * @param item
     */
    public void getAdditional(ItemModel item) {
        List<ItemModel> recommends = mDataManager.getRealmHelper().getRecommendItems(item);
        if ((recommends.size() == 0) && (item.getBuy_now() == 1)) {
            // Нет рекомендованных товаров и включена покупка в один клик
            // В корзину
            addToBasket(item, 1, new ArrayList<>());
        } else {
            getMvpView().showAdditional(item, recommends);
        }
    }

    /**
     * Добавление товаров в корзину
     *
     * @param item
     * @param count
     * @param recommend
     */
    public void addToBasket(ItemModel item, int count, List<Integer> recommend) {
        getMvpView().addBasketEvent(mDataManager.getRealmHelper().addToBasket(item, count, recommend));
        getMvpView().updateBasketItemCount(mDataManager.getRealmHelper().getBasketCount());
    }

}

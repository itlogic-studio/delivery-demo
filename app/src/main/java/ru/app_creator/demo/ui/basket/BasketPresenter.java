package ru.app_creator.demo.ui.basket;

import javax.inject.Inject;

import ru.app_creator.demo.data.DataManager;
import ru.app_creator.demo.ui.base.BasePresenter;
import rx.Subscription;

public class BasketPresenter extends BasePresenter<BasketMvpView> {

    private final DataManager mDataManager;
    private Subscription mSubscription;

    @Inject
    public BasketPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(BasketMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
        if (mSubscription != null) mSubscription.unsubscribe();
    }

}

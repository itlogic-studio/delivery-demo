package ru.app_creator.demo.ui.main.additional;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.app_creator.demo.R;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.injection.ActivityContext;
import ru.app_creator.demo.ui.main.MainActivity;

public class RecommendDialogFactory implements RecommendAdapter.OnItemSelectListener {

    private int count;
    private List<Integer> selectedIds;

    private RecommendAdapter adapter;
    private Context context;
    private AlertDialog alertDialog;

    @BindView(R.id.recommendList) RecyclerView itemList;
    @BindView(R.id.wrapCount) View wrapCount;
    @BindView(R.id.wrapRecommend) View wrapRecommend;
    @BindView(R.id.countText) TextView countText;
    @BindView(R.id.incCount) Button incCount;
    @BindView(R.id.decCount) Button decCount;

    @Inject public RecommendDialogFactory(RecommendAdapter adapter, @ActivityContext Context context) {
        this.adapter = adapter;
        this.context = context;
    }

    public interface OnRecommendDialogEventListener {
        void onSelect(int count, List<Integer> recommend);
    }

    public void show(ItemModel item, List<ItemModel> recommendItems, OnRecommendDialogEventListener onRecommendDialogEventListener) {
        ((MainActivity) context).activityComponent().inject(this);
        count = 1;
        selectedIds = new ArrayList<>();
        AlertDialog.Builder dialog = new AlertDialog.Builder(context, R.style.AlertDialogStyle);
        final View dialogView = LayoutInflater.from(context).inflate(R.layout.dialod_additional, null);
        ButterKnife.bind(this, dialogView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        itemList.setHasFixedSize(true);
        itemList.setLayoutManager(layoutManager);
        dialog.setView(dialogView);
        countText.setText(String.valueOf(count));
        incCount.setOnClickListener(v -> {
            count++;
            countText.setText(String.valueOf(count));
        });
        decCount.setOnClickListener(v -> {
            if (count > 1) {
                count--;
                countText.setText(String.valueOf(count));
            }
        });
        if (recommendItems.size() > 0) {
            // Есть рекомендованные товары
            wrapCount.setVisibility((item.getBuy_now() == 1) ? View.GONE : View.VISIBLE);
            adapter.init(R.layout.item_recommend_single);
            adapter.setDataSet(recommendItems);
            adapter.setOnItemSelectListener(this);
            itemList.setAdapter(adapter);
        } else {
            wrapRecommend.setVisibility(View.GONE);
        }
        dialog.setPositiveButton("В корзину", (dialog1, which) -> {
            if (onRecommendDialogEventListener != null) {
                onRecommendDialogEventListener.onSelect(count, selectedIds);
            }
        });
        dialog.setNegativeButton("Отмена", null);
        alertDialog = dialog.show();
    }

    @Override
    public void selectItem(ItemModel item, boolean selected) {
        if (selected) {
            selectedIds.add(item.getId());
        } else {
            selectedIds.remove((Integer) item.getId());
        }
    }

    /**
     * Закрывает диалоговое окно
     */
    public void dismissDialog() {
        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }
    }

}

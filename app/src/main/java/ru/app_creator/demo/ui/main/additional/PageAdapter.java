package ru.app_creator.demo.ui.main.additional;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ru.app_creator.demo.data.model.GroupModel;

public class PageAdapter extends FragmentPagerAdapter {

    private List<GroupModel> dataSet;

    public PageAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        dataSet = new ArrayList<>();
    }

    public void setDataSet(List<GroupModel> data) {
        dataSet = data;
    }

    @Override
    public Fragment getItem(int position) {
        return ItemsListFragment.newInstance(dataSet.get(position).getId(), dataSet.get(position).getView_type());
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return dataSet.get(position).getName();
    }
}

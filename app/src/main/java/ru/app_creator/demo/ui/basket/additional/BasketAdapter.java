package ru.app_creator.demo.ui.basket.additional;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.app_creator.demo.R;
import ru.app_creator.demo.data.model.ImageModel;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.injection.ActivityContext;
import ru.app_creator.demo.util.Helper;

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.ViewHolder> {

    private Context context;
    private List<ItemModel> dataSet;

    @Inject public BasketAdapter(@ActivityContext Context context) {
        this.context = context;
        dataSet =  new ArrayList<>();
    }

    public void setDataSet(List<ItemModel> data) {
        dataSet = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BasketAdapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_basket, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemModel item = dataSet.get(position);
        holder.textTitle.setText(item.getName());
        holder.textDesc.setText(item.getDesc());
        holder.textPrice.setText((item.getPrice() == 0) ? "Бесплатно" : Helper.formatCurrency("руб.", item.getPrice()));
        holder.textSum.setText(Helper.formatCurrency("руб.", item.getPrice() * item.getBasketCount()));
        holder.textCount.setText(item.getBasketCount() + " шт.");
        List<ImageModel> images = item.getImages();
        if (images.size() > 0) {
            Glide.with(context)
                    .load(images.get(0).getUrl_small())
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(holder.image);
        }
        holder.btnDelete.setOnClickListener(v -> {


        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image) ImageView image;
        @BindView(R.id.textTitle) TextView textTitle;
        @BindView(R.id.textDesc) TextView textDesc;
        @BindView(R.id.textPrice) TextView textPrice;
        @BindView(R.id.textCount) TextView textCount;
        @BindView(R.id.textSum) TextView textSum;
        @BindView(R.id.btnDelete) View btnDelete;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

package ru.app_creator.demo.ui.main.additional;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.app_creator.demo.R;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.injection.ActivityContext;
import ru.app_creator.demo.util.Helper;
import ru.app_creator.demo.util.ui.SmoothCheckBox;

public class RecommendAdapter extends RecyclerView.Adapter<RecommendAdapter.ViewHolder> {

    private List<ItemModel> dataSet;
    private int resource_id;
    private Context context;
    private OnItemSelectListener itemSelectListener;

    public interface OnItemSelectListener {
        void selectItem(ItemModel item, boolean selected);
    }

    public void setOnItemSelectListener(OnItemSelectListener itemSelectListener) {
        this.itemSelectListener = itemSelectListener;
    }

    @Inject
    public RecommendAdapter(@ActivityContext Context context) {
        this.context = context;
        dataSet = new ArrayList<>();
    }

    public void init(int resource_id) {
        this.resource_id = resource_id;
    }

    public void setDataSet(List<ItemModel> dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(resource_id, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemModel item = dataSet.get(position);
        holder.title.setText(item.getName());
        holder.desc.setText(item.getDesc());
        holder.price.setText(Helper.formatCurrency("руб.", item.getPrice()));
        if (item.getImages().size() > 0) {
            Glide.with(context)
                    .load(item.getImages().get(0).getUrl_small())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(holder.image);
        }
        holder.selector.setChecked(item.isSelected());
        holder.wrap.setOnClickListener(v -> holder.selector.setChecked(!holder.selector.isChecked(), true));
        holder.selector.setOnCheckedChangeListener((checkBox, isChecked) -> {
            item.setSelected(isChecked);
            if (itemSelectListener != null) {
                itemSelectListener.selectItem(item, isChecked);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    /**
     * ViewHolder
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textTitle) TextView title;
        @BindView(R.id.textDesc) TextView desc;
        @BindView(R.id.textPrice) TextView price;
        @BindView(R.id.image) ImageView image;
        @BindView(R.id.select) SmoothCheckBox selector;
        @BindView(R.id.itemRecommend) View wrap;

        private ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

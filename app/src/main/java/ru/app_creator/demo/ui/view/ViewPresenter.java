package ru.app_creator.demo.ui.view;

import java.util.List;

import javax.inject.Inject;

import ru.app_creator.demo.data.DataManager;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.ui.base.BasePresenter;

public class ViewPresenter extends BasePresenter<ViewMvpView> {

    private final DataManager mDataManager;

    @Inject
    public ViewPresenter(DataManager dataManager) {
        mDataManager = dataManager;
    }

    @Override
    public void attachView(ViewMvpView mvpView) {
        super.attachView(mvpView);
    }

    @Override
    public void detachView() {
        super.detachView();
    }

    /**
     * Получим айтем
     *
     * @param item_id
     */
    public void getItem(int item_id) {
        checkViewAttached();
        ItemModel item = mDataManager.getRealmHelper().getItemById(item_id);
        getMvpView().showItem(item, mDataManager.getRealmHelper().getRecommendItems(item));
    }

    /**
     * Добавление товаров в корзину
     *
     * @param item
     * @param count
     * @param recommend
     */
    public void addToBasket(ItemModel item, int count, List<Integer> recommend) {
        getMvpView().addBasketEvent(mDataManager.getRealmHelper().addToBasket(item, count, recommend));
        getMvpView().updateBasketItemCount(mDataManager.getRealmHelper().getBasketCount());
    }
}

package ru.app_creator.demo.ui.main.additional;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.app_creator.demo.R;
import ru.app_creator.demo.data.DataManager;
import ru.app_creator.demo.ui.main.MainActivity;
import ru.app_creator.demo.util.RxUtil;
import ru.app_creator.demo.util.ui.AutoFitRecyclerView;
import rx.Subscription;

public class ItemsListFragment extends Fragment {

    @BindView(R.id.items_list) AutoFitRecyclerView itemsListsView;
    @Inject DataManager mDataManager;
    @Inject ItemsListAdapter mItemsListAdapter;

    private Subscription mSubscription;

    public static ItemsListFragment newInstance(int group_id, int view_type) {
        ItemsListFragment fragment = new ItemsListFragment();
        Bundle b = new Bundle();
        b.putInt("group_id", group_id);
        b.putInt("view_type", view_type);
        fragment.setArguments(b);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_items, null);
        ButterKnife.bind(this, view);
        ((MainActivity) getActivity()).activityComponent().inject(this);
        int itemLayout = R.layout.item_vertical;
        switch (this.getArguments().getInt("view_type")) {
            case 1:
                itemLayout = R.layout.item_horizontal;
                itemsListsView.setColumnWidth(getResources().getInteger(R.integer.column_width_horizontal));
                break;
            case 2:
                itemLayout = R.layout.item_vertical;
                itemsListsView.setColumnWidth(getResources().getInteger(R.integer.column_width_vertical));
                break;
        }
        mItemsListAdapter.init(getContext(), itemLayout);
        itemsListsView.setAdapter(mItemsListAdapter);
        RxUtil.unsubscribe(mSubscription);
        mSubscription = mDataManager.getRealmHelper().getItemByGroupById(this.getArguments().getInt("group_id"))
                .subscribe(items -> {
                    mItemsListAdapter.setDataSet(items);
                    mItemsListAdapter.notifyDataSetChanged();
                });
        return view;
    }
}

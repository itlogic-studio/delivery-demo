package ru.app_creator.demo.ui.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.app_creator.demo.R;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.ui.base.BaseActivity;
import ru.app_creator.demo.ui.basket.BasketActivity;
import ru.app_creator.demo.ui.view.additional.ImageSliderAdapter;
import ru.app_creator.demo.util.DialogFactory;
import ru.app_creator.demo.util.Helper;
import ru.app_creator.demo.util.ui.AutoHeightViewPager;
import ru.app_creator.demo.util.ui.DepthPageTransformer;
import ru.app_creator.demo.util.ui.SmoothCheckBox;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ViewActivity extends BaseActivity implements ViewMvpView,
        ViewPager.OnPageChangeListener {

    private final static String EXTRA_ITEM_ID = "ru.app_creator.demo.EXTRA_ITEM";

    @Inject ViewPresenter mViewPresenter;
    @Inject ImageSliderAdapter sliderAdapter;

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.basket) View basketBtn;
    @BindView(R.id.textTitle) TextView title;
    @BindView(R.id.textFullDesc) TextView desc;
    @BindView(R.id.textPrice) TextView price;
    @BindView(R.id.scrollView) NestedScrollView scrollView;
    @BindView(R.id.imageSlider) AutoHeightViewPager imageSlider;
    @BindView(R.id.wrapRecommend) LinearLayout wrapRecommend;
    @BindView(R.id.textRecommend) View textRecommend;
    @BindView(R.id.separateRecommend) View separateRecommend;
    @BindView(R.id.viewPagerIndicator) LinearLayout pager_indicator;
    @BindView(R.id.countText) TextView countText;
    @BindView(R.id.incCount) Button incCount;
    @BindView(R.id.decCount) Button decCount;
    @BindView(R.id.addBasket) View addBasket;
    @BindView(R.id.textCount) TextView itemCountText;

    private List<Integer> recommend;
    private int dotsCount;
    private ImageView[] dots;
    private int count;

    public static Intent getStartIntent(Context context, int item_id) {
        Intent intent = new Intent(context, ViewActivity.class);
        intent.putExtra(EXTRA_ITEM_ID, item_id);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_view);
        ButterKnife.bind(this);
        mViewPresenter.attachView(this);
        mViewPresenter.getItem(getIntent().getIntExtra(EXTRA_ITEM_ID, 0));
        recommend = new ArrayList<>();
        setupUI();
    }

    public void setupUI() {
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(Color.WHITE);
        toolbar.getBackground().setAlpha(200);
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        final Drawable upArrow = ContextCompat.getDrawable(this, R.mipmap.ic_back_arrow);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        toolbar.setNavigationOnClickListener(v -> finish());
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick(R.id.basket)
    public void submit() {
        startActivityForResult(BasketActivity.getStartIntent(this), 100);
    }

    @Override
    public void showItem(ItemModel item, List<ItemModel> recommends) {
        title.setText(item.getName());
        desc.setText(item.getDesc_full());
        price.setText((item.getPrice() == 0) ? "Бесплатно" : Helper.formatCurrency("руб.", item.getPrice()));
        if (recommends.size() > 0) {
            wrapRecommend.setVisibility(View.VISIBLE);
            textRecommend.setVisibility(View.VISIBLE);
            separateRecommend.setVisibility(View.VISIBLE);
            wrapRecommend.removeAllViews();
            for (ItemModel option : recommends) {
                View child = getLayoutInflater().inflate(R.layout.item_recommend_single, null);
                SmoothCheckBox selector = (SmoothCheckBox) child.findViewById(R.id.select);
                selector.setOnCheckedChangeListener((checkBox, isChecked) -> {
                    if (isChecked) {
                        recommend.add(option.getId());
                    } else {
                        recommend.remove((Integer) option.getId());
                    }
                });
                child.setOnClickListener(v -> selector.setChecked(!selector.isChecked(), true));
                ((TextView) child.findViewById(R.id.textTitle)).setText(option.getName());
                ((TextView) child.findViewById(R.id.textDesc)).setText(option.getDesc());
                ((TextView) child.findViewById(R.id.textPrice)).setText(Helper.formatCurrency("руб.", option.getPrice()));

                if (option.getImages().size() > 0) {
                    Glide.with(this)
                            .load(option.getImages().get(0).getUrl_small())
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into((ImageView) child.findViewById(R.id.image));
                }
                wrapRecommend.addView(child);
            }
        } else {
            wrapRecommend.setVisibility(View.GONE);
            textRecommend.setVisibility(View.GONE);
            separateRecommend.setVisibility(View.GONE);
        }

        imageSlider.setPageTransformer(true, new DepthPageTransformer());
        imageSlider.setOnTouchListener(new View.OnTouchListener() {
            int dragthreshold = 30;
            int downX;
            int downY;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        downX = (int) event.getRawX();
                        downY = (int) event.getRawY();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        int distanceX = Math.abs((int) event.getRawX() - downX);
                        int distanceY = Math.abs((int) event.getRawY() - downY);

                        if (distanceY > distanceX && distanceY > dragthreshold) {
                            imageSlider.getParent().requestDisallowInterceptTouchEvent(false);
                            scrollView.getParent().requestDisallowInterceptTouchEvent(true);
                        } else if (distanceX > distanceY && distanceX > dragthreshold) {
                            imageSlider.getParent().requestDisallowInterceptTouchEvent(true);
                            scrollView.getParent().requestDisallowInterceptTouchEvent(false);
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        scrollView.getParent().requestDisallowInterceptTouchEvent(false);
                        imageSlider.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
        sliderAdapter.setDataSet(item.getImages());
        sliderAdapter.notifyDataSetChanged();
        imageSlider.setAdapter(sliderAdapter);
        imageSlider.addOnPageChangeListener(this);

        dotsCount = sliderAdapter.getCount();
        if (dotsCount > 0) {
            pager_indicator.setVisibility((dotsCount == 1) ? View.GONE : View.VISIBLE);
            dots = new ImageView[dotsCount];
            for (int i = 0; i < dotsCount; i++) {
                dots[i] = new ImageView(this);
                dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.viewpager_indicator));
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(4, 0, 4, 0);
                pager_indicator.addView(dots[i], params);
            }
            dots[0].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.viewpager_indicator_select));
        }
        // Выбранное кол-во
        count = 1;
        countText.setText(String.valueOf(count));
        incCount.setOnClickListener(v -> {
            count++;
            countText.setText(String.valueOf(count));
        });
        decCount.setOnClickListener(v -> {
            if (count > 1) {
                count--;
                countText.setText(String.valueOf(count));
            }
        });
        addBasket.setOnClickListener(v -> mViewPresenter.addToBasket(item, count, recommend));
    }

    @Override
    public void addBasketEvent(int count) {
        DialogFactory.showMessage(this,"Товары добавлены в корзину - " + String.valueOf(count) + " шт.");
    }

    @Override
    public void updateBasketItemCount(int count) {
        itemCountText.setText(String.valueOf(count));
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.viewpager_indicator));
        }
        dots[position].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.viewpager_indicator_select));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}

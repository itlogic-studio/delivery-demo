package ru.app_creator.demo.ui.basket;

import java.util.List;

import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.ui.base.MvpView;

public interface BasketMvpView extends MvpView {

    void showData(List<ItemModel> items);
    void showDataEmpty();
}

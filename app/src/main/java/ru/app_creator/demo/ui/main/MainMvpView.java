package ru.app_creator.demo.ui.main;

import java.util.List;

import ru.app_creator.demo.data.model.GroupModel;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    void showData(List<GroupModel> data);
    void showDataEmpty();
    void showAdditional(ItemModel item, List<ItemModel> recommendItems);
    void addBasketEvent(int count);
    void updateBasketItemCount(int count);
}

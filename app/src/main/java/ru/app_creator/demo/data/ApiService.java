package ru.app_creator.demo.data;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import io.realm.RealmObject;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import ru.app_creator.demo.BuildConfig;
import ru.app_creator.demo.data.json.FcmTokenModel;
import ru.app_creator.demo.data.json.VersionModel;
import ru.app_creator.demo.data.model.GroupModel;
import rx.Observable;

public interface ApiService {

    String ENDPOINT = "http://192.168.10.39/api/v1/app/";

    @GET("version")
    Observable<VersionModel> getVersion();

    @GET("items")
    Observable<List<GroupModel>> getData();

    @POST("fcm")
    @FormUrlEncoded
    Observable<FcmTokenModel> registerFcmToken(@Field("token") String token, @Field("device_id") String device_id);

    class Factory {
        public static ApiService makeItemsService() {
            Gson gson = new GsonBuilder().setExclusionStrategies(new  ExclusionStrategy() {
                @Override
                public boolean shouldSkipField(FieldAttributes f) {
                    return f.getDeclaringClass().equals(RealmObject.class);
                }

                @Override
                public boolean shouldSkipClass(Class<?> clazz) {
                    return false;
                }
            }).create();

            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiService.ENDPOINT)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(ApiService.class);
        }
    }
}

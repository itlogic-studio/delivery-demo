package ru.app_creator.demo.data.local;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import ru.app_creator.demo.data.model.GroupModel;
import ru.app_creator.demo.data.model.ItemModel;
import ru.app_creator.demo.data.model.RecommendModel;
import rx.Observable;

@Singleton
public class RealmHelper {

    private final Realm realm;

    @Inject
    public RealmHelper() {
        realm = Realm.getDefaultInstance();
    }

    public Realm getRealm() {
        return realm;
    }

    public void setAllData(final List<GroupModel> data) {
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        realm.executeTransaction(realm -> realm.copyToRealm(data));
    }

    /**
     * Возвращает список групп
     *
     * @return
     */
    public Observable<RealmResults<GroupModel>> getAllGroup() {
        return realm.where(GroupModel.class)
                .equalTo("visible", 1)
                .findAllAsync().asObservable();
    }

    /**
     * Возвращает список айтемов
     *
     * @param group_id
     * @return
     */
    public Observable<RealmResults<ItemModel>> getItemByGroupById(int group_id) {
        return realm.where(ItemModel.class)
                .equalTo("group_id", group_id)
                .findAllAsync().asObservable();
    }

    /**
     * Получим айтем по id
     *
     * @param item_id
     * @return
     */
    public ItemModel getItemById(int item_id) {
        return realm.where(ItemModel.class)
                .equalTo("id", item_id)
                .findFirst();
    }

    /**
     * Список рекомендованных товаров
     *
     * @param item
     * @return
     */
    public RealmResults<ItemModel> getRecommendItems(ItemModel item) {
        RealmList<RecommendModel> recommends = item.getRecommend();
        RealmQuery<ItemModel> query;
        if (recommends.size() > 0) {
            query = realm.where(ItemModel.class);
            boolean first = true;
            for (RecommendModel r_item : recommends) {
                if (!first) {
                    query = query.or();
                } else {
                    first = false;
                }
                query = query.equalTo("id", r_item.getItem_id());
            }
        } else {
            // Заглушка
            query = realm.where(ItemModel.class).equalTo("id", 0);
        }
        return query.findAll();
    }

    /**
     * Добавляем товар в корзину
     *
     * @param item
     * @param count
     * @param recommend
     */
    public int addToBasket(ItemModel item, int count, List<Integer> recommend) {
        realm.beginTransaction();
        item.setBasketCount(item.getBasketCount() + count);
        // Добавим рекомендованные товары
        for (Integer item_id : recommend) {
            ItemModel option = realm.where(ItemModel.class).equalTo("id", item_id).findFirst();
            option.setBasketCount(option.getBasketCount() + 1);
        }
        realm.commitTransaction();
        return count + recommend.size();
    }

    /**
     * Возвращает кол-во товаров в корзине
     *
     * @return
     */
    public int getBasketCount() {
        RealmResults<ItemModel> results = realm.where(ItemModel.class).greaterThan("basketCount", 0).findAll();
        return results.sum("basketCount").intValue();
    }
}

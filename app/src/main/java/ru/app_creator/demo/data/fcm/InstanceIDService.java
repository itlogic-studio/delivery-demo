package ru.app_creator.demo.data.fcm;

import android.provider.Settings;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.orhanobut.logger.Logger;

import javax.inject.Inject;

import ru.app_creator.demo.DeliveryApplication;
import ru.app_creator.demo.data.DataManager;
import ru.app_creator.demo.data.json.FcmTokenModel;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class InstanceIDService extends FirebaseInstanceIdService {

    @Inject DataManager mDataManager;
    private Subscription mSubscription;

    @Override
    public void onCreate() {
        super.onCreate();
        DeliveryApplication.get(this).getComponent().inject(this);
    }

    @Override
    public void onTokenRefresh() {
        String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        if (mSubscription != null && !mSubscription.isUnsubscribed()) mSubscription.unsubscribe();
        mSubscription = mDataManager.registerFcmToken(FirebaseInstanceId.getInstance().getToken(), android_id)
                .subscribeOn(Schedulers.newThread())
                .retry(3)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<FcmTokenModel>() {
                    @Override
                    public void onCompleted() {
                        Logger.i("Fcm токен успешно зарегистрирован");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e, "Ошибка регистрации токена fcm");
                    }

                    @Override
                    public void onNext(FcmTokenModel token) {
                        mDataManager.getPreferencesHelper().setFcmAppId(token.getId());

                    }
                });
    }
}

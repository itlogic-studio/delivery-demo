package ru.app_creator.demo.data.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ImageModel extends RealmObject {

    @PrimaryKey
    private int id;

    private String url_small;
    private String url_medium;
    private String url_large;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl_small() {
        return url_small;
    }

    public void setUrl_small(String url_small) {
        this.url_small = url_small;
    }

    public String getUrl_medium() {
        return url_medium;
    }

    public void setUrl_medium(String url_medium) {
        this.url_medium = url_medium;
    }

    public String getUrl_large() {
        return url_large;
    }

    public void setUrl_large(String url_large) {
        this.url_large = url_large;
    }
}

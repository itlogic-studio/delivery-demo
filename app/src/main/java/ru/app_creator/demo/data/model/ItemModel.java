package ru.app_creator.demo.data.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class ItemModel extends RealmObject {

    @PrimaryKey
    private int id;

    private int group_id;
    private String name;
    private String desc;
    private String desc_full;
    private Double price;
    private Double price_old;
    private int visible;
    private int show_detail;
    private int buy_now;

    private RealmList<ImageModel> images;
    private RealmList<RecommendModel> recommend;

    private int basketCount;

    @Ignore
    private boolean isSelected;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDesc_full() {
        return desc_full;
    }

    public void setDesc_full(String desc_full) {
        this.desc_full = desc_full;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPrice_old() {
        return price_old;
    }

    public void setPrice_old(Double price_old) {
        this.price_old = price_old;
    }

    public int getShow_detail() {
        return show_detail;
    }

    public void setShow_detail(int show_detail) {
        this.show_detail = show_detail;
    }

    public RealmList<ImageModel> getImages() {
        return images;
    }

    public void setImages(RealmList<ImageModel> images) {
        this.images = images;
    }

    public int getBuy_now() {
        return buy_now;
    }

    public void setBuy_now(int buy_now) {
        this.buy_now = buy_now;
    }

    public int getVisible() {
        return visible;
    }

    public void setVisible(int visible) {
        this.visible = visible;
    }

    public RealmList<RecommendModel> getRecommend() {
        return recommend;
    }

    public void setRecommend(RealmList<RecommendModel> recommend) {
        this.recommend = recommend;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public int getBasketCount() {
        return basketCount;
    }

    public void setBasketCount(int basketCount) {
        this.basketCount = basketCount;
    }
}

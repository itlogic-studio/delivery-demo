package ru.app_creator.demo.data.json;

public class VersionModel {

    private int version;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}

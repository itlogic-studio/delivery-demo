package ru.app_creator.demo.data.event;

import ru.app_creator.demo.data.model.ItemModel;

public class ItemClickEvent {

    private ItemModel item;

    public ItemClickEvent(ItemModel item) {
        this.item = item;
    }

    public ItemModel getItem() {
        return item;
    }

    public void setItem(ItemModel item) {
        this.item = item;
    }
}

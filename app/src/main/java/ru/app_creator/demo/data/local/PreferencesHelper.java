package ru.app_creator.demo.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.app_creator.demo.injection.ApplicationContext;

@Singleton
public class PreferencesHelper {

    private final SharedPreferences settings;

    private static final String APP_PREFERENCES  = "delivery";
    private static final String DATA_VERSION     = "data_version";
    private static final String FCM_APP_ID       = "fcm_app_id";
    private static final String CUSTOMER_NAME    = "customer_name";
    private static final String CUSTOMER_PHONE   = "customer_phone";
    private static final String CUSTOMER_ADDRESS = "customer_address";

    @Inject
    public PreferencesHelper(@ApplicationContext Context context) {
        settings = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
    }

    /**
     * Версионность набора данных
     *
     * @param value
     */
    public void setDataVersion(int value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(DATA_VERSION, value);
        editor.apply();
    }

    public int getDataVersion() {
        return settings.getInt(DATA_VERSION, 0);
    }

    /**
     * ID клиента после регистрации fcm токена
     *
     * @param value
     */
    public void setFcmAppId(String value) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(FCM_APP_ID, value);
        editor.apply();
    }

    public String getFcmAppId() {
        return settings.getString(FCM_APP_ID, "");
    }

    /**
     * Данные заказчика
     *
     * @param name
     * @param phone
     * @param address
     */
    public void setCustomerData(String name, String phone, String address) {
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(CUSTOMER_NAME, name);
        editor.putString(CUSTOMER_PHONE, phone);
        editor.putString(CUSTOMER_ADDRESS, address);
        editor.apply();
    }

    public String getCustomerName() {
        return settings.getString(CUSTOMER_NAME, "");
    }
    public String getCustomerPhone() {
        return settings.getString(CUSTOMER_PHONE, "");
    }
    public String getCustomerAddress() {
        return settings.getString(CUSTOMER_ADDRESS, "");
    }
}

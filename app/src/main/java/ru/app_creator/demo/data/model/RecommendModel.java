package ru.app_creator.demo.data.model;

import io.realm.RealmObject;

public class RecommendModel extends RealmObject {
    private int item_id;

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

}

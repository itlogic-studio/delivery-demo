package ru.app_creator.demo.data;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.orhanobut.logger.Logger;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import javax.inject.Inject;

import ru.app_creator.demo.DeliveryApplication;
import ru.app_creator.demo.data.event.ServiceErrorEvent;
import ru.app_creator.demo.data.json.VersionModel;
import ru.app_creator.demo.data.model.GroupModel;
import ru.app_creator.demo.util.AndroidComponentUtil;
import ru.app_creator.demo.util.NetworkUtil;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SyncService extends Service {

    @Inject DataManager mDataManager;
    private Subscription mSubscription;
    private int dataVersion;

    public static Intent getStartIntent(Context context) {
        return new Intent(context, SyncService.class);
    }

    public static boolean isRunning(Context context) {
        return AndroidComponentUtil.isServiceRunning(context, SyncService.class);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        DeliveryApplication.get(this).getComponent().inject(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, final int startId) {
        Logger.i("Старт синхронизации ...");
        dataVersion = 0;
        if (!NetworkUtil.isNetworkConnected(this)) {
            Logger.i("Синхронизация отменена, интернет соединение не доступно");
            AndroidComponentUtil.toggleComponent(this, SyncOnConnectionAvailable.class, true);
            stopSelf(startId);
            return START_NOT_STICKY;
        }
        checkVersionData(startId);
        return START_STICKY;
    }

    /**
     * Проверка версии набора данных
     *
     * @param startId
     */
    private void checkVersionData(final int startId) {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) mSubscription.unsubscribe();
        mSubscription = mDataManager.syncVersionData()
                .subscribeOn(Schedulers.newThread())
                .retry(3)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<VersionModel>() {
                    @Override
                    public void onCompleted() {
                        Logger.i("Версия данных успешно синхронизирована");
                        if (dataVersion != mDataManager.getPreferencesHelper().getDataVersion()) {
                            loadData(startId);
                        } else {
                            Logger.i("Данные не изменились");
                            stopSelf(startId);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e, "Ошибка загрузки версии набора данных");
                        EventBus.getDefault().post(new ServiceErrorEvent(e));
                        stopSelf(startId);
                    }

                    @Override
                    public void onNext(VersionModel version) {
                        dataVersion = version.getVersion();
                    }
                });
    }

    /**
     * Загрузка данных
     *
     * @param startId
     */
    private void loadData(final int startId) {
        if (mSubscription != null && !mSubscription.isUnsubscribed()) mSubscription.unsubscribe();
        mSubscription = mDataManager.syncAllData()
                .subscribeOn(Schedulers.newThread())
                .retry(3)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<GroupModel>>() {
                    @Override
                    public void onCompleted() {
                        Logger.i("Набор данных успешно синхронизирован");
                        mDataManager.getPreferencesHelper().setDataVersion(dataVersion);
                        stopSelf(startId);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(e, "Ошибка загрузки набора данных");
                        stopSelf(startId);
                    }

                    @Override
                    public void onNext(List<GroupModel> data) {
                        mDataManager.getRealmHelper().setAllData(data);

                    }
                });
    }

    @Override
    public void onDestroy() {
        if (mSubscription != null) mSubscription.unsubscribe();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public static class SyncOnConnectionAvailable extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)
                    && NetworkUtil.isNetworkConnected(context)) {
                Logger.i("Соединение восстановлено, перезапуск синхронизации ...");
                AndroidComponentUtil.toggleComponent(context, this.getClass(), false);
                context.startService(getStartIntent(context));
            }
        }
    }
}

package ru.app_creator.demo.data;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.app_creator.demo.data.json.FcmTokenModel;
import ru.app_creator.demo.data.json.VersionModel;
import ru.app_creator.demo.data.local.PreferencesHelper;
import ru.app_creator.demo.data.local.RealmHelper;
import ru.app_creator.demo.data.model.GroupModel;
import rx.Observable;

@Singleton
public class DataManager {

    private final PreferencesHelper mPreferencesHelper;
    private final RealmHelper mRealmHelper;
    private final ApiService mApiService;

    @Inject public DataManager(PreferencesHelper preferencesHelper, RealmHelper realmHelper, ApiService apiService) {
        mPreferencesHelper = preferencesHelper;
        mRealmHelper = realmHelper;
        mApiService = apiService;
    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    public RealmHelper getRealmHelper() {
        return mRealmHelper;
    }

    /**
     * Версия набора данных
     *
     * @return
     */
    public Observable<VersionModel> syncVersionData() {
        return mApiService.getVersion();
    }

    /**
     * Набор данных
     *
     * @return
     */
    public Observable<List<GroupModel>> syncAllData() {
        return mApiService.getData();
    }

    /**
     * Регистрация fcm ключа на сервере
     *
     * @param token
     * @param device_id
     * @return
     */
    public Observable<FcmTokenModel> registerFcmToken(String token, String device_id) {
        return mApiService.registerFcmToken(token, device_id);
    }
}

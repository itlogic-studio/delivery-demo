package ru.app_creator.demo.data.event;

public class ServiceErrorEvent {
    private Throwable e;

    public ServiceErrorEvent(Throwable e) {
        this.e = e;
}

    public Throwable getError() {
        return e;
    }
}

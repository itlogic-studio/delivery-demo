package ru.app_creator.demo.injection.component;

import dagger.Subcomponent;
import ru.app_creator.demo.injection.PerActivity;
import ru.app_creator.demo.injection.module.ActivityModule;
import ru.app_creator.demo.ui.basket.BasketActivity;
import ru.app_creator.demo.ui.main.additional.ItemsListFragment;
import ru.app_creator.demo.ui.main.MainActivity;
import ru.app_creator.demo.ui.main.additional.RecommendDialogFactory;
import ru.app_creator.demo.ui.splash.SplashActivity;
import ru.app_creator.demo.ui.view.ViewActivity;

@PerActivity
@Subcomponent(modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(SplashActivity splashActivity);
    void inject(MainActivity mainActivity);
    void inject(ViewActivity viewActivity);
    void inject(BasketActivity basketActivity);
    void inject(ItemsListFragment itemsListFragment);
    void inject(RecommendDialogFactory recommendDialogFactory);

}
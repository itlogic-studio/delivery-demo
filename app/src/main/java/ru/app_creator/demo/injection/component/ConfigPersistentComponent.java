package ru.app_creator.demo.injection.component;

import dagger.Component;
import ru.app_creator.demo.injection.ConfigPersistent;
import ru.app_creator.demo.injection.module.ActivityModule;

@ConfigPersistent
@Component(dependencies = ApplicationComponent.class)
public interface ConfigPersistentComponent {

    ActivityComponent activityComponent(ActivityModule activityModule);

}

package ru.app_creator.demo.injection.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import ru.app_creator.demo.data.DataManager;
import ru.app_creator.demo.data.SyncService;
import ru.app_creator.demo.data.fcm.InstanceIDService;
import ru.app_creator.demo.injection.ApplicationContext;
import ru.app_creator.demo.injection.module.ApplicationModule;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(SyncService syncService);
    void inject(InstanceIDService instanceIDService);

    @ApplicationContext
    Context context();
    Application application();
//    ApiService apiService();
    //PreferencesHelper preferencesHelper();
    //DatabaseHelper databaseHelper();
    DataManager dataManager();

    //RxEventBus eventBus();
}
